# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 17:42:12 2020

@author: ssai
"""

import io
import logging
from flask import Flask, jsonify, request
import boto3
from flask_cors import CORS
from gevent.pywsgi import WSGIServer
from botocore.exceptions import ClientError
from datetime import datetime

APP = Flask(__name__)
CORS(APP)
DBRESOURCE = boto3.resource("dynamodb")
S3FILES = boto3.client("s3")

@APP.route("/signin", methods=["POST", "GET"])
def newregister():
    '''getting data from registration '''
    if(request.method == "POST"):
        formdata = request.get_json()
        Name = formdata.get("name")
        Email = formdata.get("email")
        Password = formdata.get("password")
        Address = formdata.get("location")
        Mobile = formdata.get("mobile")
        Gender = formdata.get("gender")
        role = formdata.get("role")
        r_date = datetime.now().strftime('%Y-%m-%d')
#        profileimg = request.files.get("photo")
#        photo = fileupload(profileimg)
#        resume = request.files.get("resume")
#        resume1 = fileupload(resume)
        g_id = int(datetime.timestamp(datetime.now()))
        print(Name,Email,Password,Address,Mobile,Gender,role,g_id,r_date)
        statuss=register(Name,Email,Password,Address,Mobile,Gender,role,g_id,r_date)
        credstatus= cred(Email,Password,role)
        print(statuss,credstatus)
    return jsonify(sucess="S")

def register(name, email, password, address, mobile, gender, role, g_id,rdate):
    '''insert data in to tables '''
    a = DBRESOURCE.Table('Student_Details')
    b = DBRESOURCE.Table('Tutor_Details')
    if(role == 'Tutor'):
        table = b
        item={
                "Tutor_Email":email,
                "Tutor_Name":name,
                "Tutor_Mobile":mobile,
                "password":password,
                "Gender":gender,
                "Course_Id":[],
                "Address":address,
#                "Pan":pan,
                "Registered_Date":rdate,
#                "photo":photo
                }
    else:
        table = a
        item={
                "Student_Email":email,
                "Student_Name":name,
                "Student_Mobile":mobile,
                "Password":password,
                "Gender":gender,
                "CourseIds":[],
                "Address":address,
                "Bank_Details":{},
                "Registered_date":rdate,
#                "Student_Photo":photo
                }
    try:
        table.put_item(
            Item=item)
        return "Sucess"
    except ClientError as e:
        logging.error(e)
        return jsonify(Failure="F")

def fileupload(file):
    '''
    file upload to s3 bucket
    '''
    s3_bucket = 'projectfileupload0245'
    object_name = file.filename
    file_name = io.BytesIO(file.stream.read())
    try:
        url = "https://projectfileupload0245.s3.ap-south-1.amazonaws.com/"+file.filename
        logging.basicConfig(level=logging.DEBUG, format='%(levelname)s: %(asctime)s: %(message)s')
        response = S3FILES.upload_fileobj(file_name, s3_bucket, object_name)
        print(response)
        return url
    except ClientError as e:
        logging.error(e)
        return False
    return jsonify(fileupload='sucess')

def cred(email,password,role):
    credtable = DBRESOURCE.Table("Credentials")
    credtable.put_item(
        Item ={
                "Email_Id":email,
                "Password":password,
                "Role":role
                })
    return "Sucess"

if __name__ == '__main__':
    PORT = 8089
    HTTP_SERVER = WSGIServer(('0.0.0.0', PORT), APP)
    print('Running on', PORT, '...')
    HTTP_SERVER.serve_forever()
