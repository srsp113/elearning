# -*- coding: utf-8 -*-
"""
Created on Tue Mar 17 10:56:38 2020

@author: ssai
"""

import boto3
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key
from flask import Flask, request, jsonify
from gevent.pywsgi import WSGIServer
from flask_cors import CORS
from datetime import datetime

APP = Flask(__name__)
CORS(APP)
DBRESOURCE = boto3.resource("dynamodb")
S3BUCKET = boto3.client("s3")

@APP.route("/topic", methods =['POST', 'GET'])
def topiccheck():
    if request.method == 'POST':
        datedata = request.get_json()
        cid = datedata.get("cid")
        datesec = datedata.get("date")
        print(cid,datesec)
        coursetable = DBRESOURCE.Table('Course_Details')
        response = coursetable.query(
                KeyConditionExpression = Key("Course_Id").eq(cid))
        coursedata = response['Items']
        print(coursedata)
        schedule = coursedata[0]['Course_Schedule']
        for i in schedule:
            if(datesec == schedule[i]):
                print(i)
                return i
    return jsonify(topic=i)


if __name__ == '__main__':
    PORT = 8245
    HTTP_SERVER = WSGIServer(('0.0.0.0', PORT), APP)
    print("Running on Port is :",PORT)
    HTTP_SERVER.serve_forever()