# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 16:15:43 2020

@author: ssai
"""
import io
import logging
from flask import Flask, jsonify, request
import boto3
from flask_cors import CORS
from gevent.pywsgi import WSGIServer
from botocore.exceptions import ClientError
from datetime import datetime

app = Flask(__name__)
CORS(app)
dbdata = boto3.resource("dynamodb")
bucketdata = boto3.client("s3")

@app.route("/signin", methods=["POST", "GET"])
def newregister():
    '''getting data from registration '''
    if(request.method == "POST"):
        jsondata = request.get_json()
        print(jsondata)
        Name = request.form.get("name")
        Email = request.form.get("email")
        Password = request.form.get("password")
        Address = request.form.get("address")
        Mobile = request.form.get("mobile")
        role = request.form.get("role")
        profileimg = request.files.get("photo")
        photo = fileupload(profileimg)
        g_id = int(datetime.timestamp(datetime.now()))
        print(Name, Email, Password, Address, Mobile, role, photo, g_id)
        register(Name, Email, Password, Address, Mobile, role, photo, g_id)
        cred=credentials(Email, Password, role, g_id)
        print(cred)
    return jsonify(sucess="S")

def register(name, email, password, address, mobile, role, photo, g_id):
    '''insert data in to tables '''
    a = dbdata.Table('StudentPersonalInfo.Dev')
    b = dbdata.Table('TutorPersonalInfo.Dev')
    if(role == 'Tutor'):
        roleofid = 'Tutor_Id'
        table = b
    else:
        roleofid = "Student_Id"
        table = a
    try:
        table.put_item(
            Item={
                roleofid:g_id,
                'Name':name,
                'Email':email,
                'Password':password,
                'Address':address,
                'Mobile':mobile,
                'Photo':photo
                })
        return jsonify(Success="S")
    except ClientError as e:
        logging.error(e)
        return jsonify(Failure="F")

def fileupload(file):
    '''
    file upload to s3 bucket
    '''
    s3_bucket = 'projectfileupload0245'
    object_name = file.filename
    file_name = io.BytesIO(file.stream.read())
    try:
        url = "https://projectfileupload0245.s3.ap-south-1.amazonaws.com/"+file.filename
        logging.basicConfig(level=logging.DEBUG, format='%(levelname)s: %(asctime)s: %(message)s')
        response = bucketdata.upload_fileobj(file_name, s3_bucket, object_name)
        print(response)
        return url
    except ClientError as e:
        logging.error(e)
        return False
    return jsonify(fileupload='sucess')

def credentials(Email,password,role,g_id):
    table=dbdata.Table('Credentials.Dev')
    table.put_item(
            Item={
                    'ID':g_id,
                    'Role':role,
                    'UserName':Email,
                    'password':password
                })
    return "Sucess"

if __name__ == '__main__':
    PORT = 8089
    HTTP_SERVER = WSGIServer(('0.0.0.0', PORT), app)
    print('Running on', PORT, '...')
    HTTP_SERVER.serve_forever()
