# -*- coding: utf-8 -*-
"""
Created on Fri Mar  6 12:37:32 2020

@author: ssai
"""

import logging
from flask import Flask, jsonify, request
import boto3
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key
from gevent.pywsgi import WSGIServer
from flask_cors import CORS

APP = Flask(__name__)
CORS(APP)
DBRESOURCE = boto3.resource('dynamodb')
S3FILES = boto3.client('s3')
listss = []
s_data = []
@APP.route('/getstudents', methods=['POST', 'GET'])
def getstu_details():
    if request.method == 'POST':
        t_id = request.form.get('tid')
        table = DBRESOURCE.Table('TutorCourses.Dev')
        response = table.get_item(
            Key={
                "Tutor_Id":int(t_id)
                })
        t_data = response['Item']
        stu_list = student_list(t_data)
    return stu_list

def student_list(tdata):
    print(tdata)
    for divide in tdata['Courses']:
        language = divide['Skill']
        courseids = divide['CourseId']
        students = divide['Students']
        hello = student(students)
        listss.append({"CourseId":courseids, "Students_list":hello, "Skill":language})
    return jsonify(listss)

def student(sids):
    print(sids)
    s_data = []
    stu_table = DBRESOURCE.Table('StudentPersonalInfo.Dev')
    for i_id in sids:
        response = stu_table.query(
            KeyConditionExpression = Key("Student_Id").eq(i_id)
            )
        stu_data = response['Items']
        s_data.append({i_id:stu_data[0]['Name']})
    return s_data

if __name__ == '__main__':
    PORT = 8089
    HTTP_SERVER = WSGIServer(('0.0.0.0', PORT), APP)
    print('Running on port is', PORT)
    HTTP_SERVER.serve_forever()
