# -*- coding: utf-8 -*-
"""
Created on Fri Mar  6 09:48:13 2020

@author: ssai
"""

import logging
import boto3
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key
from flask import Flask, request, jsonify
from gevent.pywsgi import WSGIServer
from flask_cors import CORS

APP = Flask(__name__)
CORS(APP)
DBRESOURCE = boto3.resource('dynamodb')
S3FILES = boto3.client('s3')

@APP.route('/login', methods=['POST', 'GET'])
def userlogin():
    if request.method == 'POST':
        jsondata = request.get_json()
        print(jsondata)
        username = jsondata.get('user')
        password = jsondata.get('passwd')
        print(username,password)
        
        try:
            table = DBRESOURCE.Table('Credentials.Dev')
            response = table.query(
                KeyConditionExpression=Key('User').eq(username)
                )
            item = response['Items']
            print(item)
        except ClientError as e:
            logging.error(e)
            return "Something Went Wrong"
        u_name = item[0]['User']
        passw = item[0]['Password']
        role = item[0]['Role']
        u_id = item[0]['ID']
        if(username == u_name and password == passw):
            details = profile(u_id, role)
            print("main function printing details ", details)
#            return jsonify(details, item)
            return jsonify(details)
        else:
            print("some thing went wrong")
            return 'Failure'
    return

def profile(u_id, role):
    print(u_id, role)
    if(role == 'Student'):
        detail_table = DBRESOURCE.Table("StudentPersonalInfo.Dev")
        roleofid = "Student_Id"
    else:
        detail_table = DBRESOURCE.Table("TutorPersonalInfo.Dev")
        roleofid = "Tutor_Id"
    try:
        response = detail_table.get_item(
            Key={
                roleofid:u_id
                })
        details = response['Item']
        print(details)
        return details
    except ClientError as e:
        logging.error(e)
        return 'Failure'

if __name__ == '__main__':
    PORT = 8089
    HTTP_SERVER = WSGIServer(('0.0.0.0', PORT), APP)
    print("Running on port", PORT)
    HTTP_SERVER.serve_forever()
