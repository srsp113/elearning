# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 10:28:20 2020

@author: ssai
"""

import io
import logging
import boto3
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Attr, Key
from flask import Flask, request, jsonify
from gevent.pywsgi import WSGIServer
from flask_cors import CORS
from datetime import datetime

APP = Flask(__name__)
CORS(APP)
DBRESOURCE = boto3.resource('dynamodb')
S3FILES = boto3.client('s3')
listofid = []
totaldetails = []
listss = []
s_data = []

@APP.route('/login', methods=['POST', 'GET'])
def userlogin():
    if request.method == 'POST':
        jsondata = request.get_json()
        print(jsondata)
        username = jsondata.get('user')
        password = jsondata.get('passwd')
        print(username,password)
        
        try:
            table = DBRESOURCE.Table('Credentials.Dev')
            response = table.query(
                KeyConditionExpression=Key('User').eq(username)
                )
            item = response['Items']
            print(item)
                
        except ClientError as e:
            logging.error(e)
            return "Something Went Wrong"
        u_name = item[0]['User']
        passw = item[0]['Password']
        role = item[0]['Role']
        u_id = item[0]['ID']
        if(username == u_name and password == passw):
            details = profile(u_id, role)
            coursedetail = coursedetails(u_id, role)
            print("main function printing details ", details)
            print("main function printing course details",coursedetail)
#            return jsonify(details, item)
            return jsonify(details, coursedetail)
        else:
            print("some thing went wrong")
            return 'Failure'
    return

def profile(u_id, role):
    print(u_id, role)
    if(role == 'Student'):
        detail_table = DBRESOURCE.Table("StudentPersonalInfo.Dev")
        roleofid = "Student_Id"
    else:
        detail_table = DBRESOURCE.Table("TutorPersonalInfo.Dev")
        roleofid = "Tutor_Id"
    try:
        response = detail_table.get_item(
            Key={
                roleofid:u_id
                })
        details = response['Item']
#       
        return details
    except ClientError as e:
        logging.error(e)
        return 'Failure'

@APP.route("/signin", methods=["POST", "GET"])
def newregister():
    '''getting data from registration '''
    if(request.method == "POST"):
        jsondata = request.get_json()
        print(jsondata)
        Name = request.form.get("name")
        Email = request.form.get("email")
        Password = request.form.get("password")
        Address = request.form.get("address")
        Mobile = request.form.get("mobile")
        role = request.form.get("role")
        profileimg = request.files.get("photo")
        photo = fileupload(profileimg)
        g_id = int(datetime.timestamp(datetime.now()))
        print(Name, Email, Password, Address, Mobile, role, photo, g_id)
        register(Name, Email, Password, Address, Mobile, role, photo, g_id)
        cred=credentials(Email, Password, role, g_id)
        print(cred)
    return jsonify(sucess="S")

def register(name, email, password, address, mobile, role, photo, g_id):
    '''insert data in to tables '''
    a = DBRESOURCE.Table('StudentPersonalInfo.Dev')
    b = DBRESOURCE.Table('TutorPersonalInfo.Dev')
    if(role == 'Tutor'):
        roleofid = 'Tutor_Id'
        table = b
    else:
        roleofid = "Student_Id"
        table = a
    try:
        table.put_item(
            Item={
                roleofid:g_id,
                'Name':name,
                'Email':email,
                'Password':password,
                'Address':address,
                'Mobile':mobile,
                'Photo':photo
                })
        return jsonify(Success="S")
    except ClientError as e:
        logging.error(e)
        return jsonify(Failure="F")

def fileupload(file):
    '''
    file upload to s3 bucket
    '''
    s3_bucket = 'projectfileupload0245'
    object_name = file.filename
    file_name = io.BytesIO(file.stream.read())
    try:
        url = "https://projectfileupload0245.s3.ap-south-1.amazonaws.com/"+file.filename
        logging.basicConfig(level=logging.DEBUG, format='%(levelname)s: %(asctime)s: %(message)s')
        response = S3FILES.upload_fileobj(file_name, s3_bucket, object_name)
        print(response)
        return url
    except ClientError as e:
        logging.error(e)
        return False
    return jsonify(fileupload='sucess')

def credentials(Email,password,role,g_id):
    table=DBRESOURCE.Table('Credentials.Dev')
    table.put_item(
            Item={
                    'ID':g_id,
                    'Role':role,
                    'User':Email,
                    'Password':password
                })
    return "Sucess"



def coursedetails(p_id, role):
    print(p_id, role)
    totaldata = []
    if(role=='Student'):
        table = DBRESOURCE.Table("StudentCourseDetail.Dev")
        try:
            response = table.get_item(
                Key={
                    'Student_Id':str(p_id)
                    })
            studentdata = response['Item']
            totaldata.append(studentdata)
            courseids = course(studentdata)
            totaldata.append(courseids)
            if(courseids):
                print(listofid)
            p_data = totaldata
        except ClientError as e:
            logging.error(e)
            return "invalid data"
    
        
    return p_data

def student_list(tdata):
    print(tdata)
    for divide in tdata['Courses']:
        language = divide['Skill']
        courseids = divide['CourseId']
        students = divide['Students']
        hello = student(students)
        listss.append({"CourseId":courseids, "Students_list":hello, "Skill":language})
    return jsonify(listss)

def student(sids):
    print(sids)
    s_data = []
    stu_table = DBRESOURCE.Table('StudentPersonalInfo.Dev')
    for i_id in sids:
        response = stu_table.query(
            KeyConditionExpression = Key("Student_Id").eq(i_id)
            )
        stu_data = response['Items']
        s_data.append({i_id:stu_data[0]['Name']})
    return s_data

def course(studentdata):
    '''checking and getting syllabus '''
    course_ids = studentdata.get('Courses')
    for i in course_ids:
        course_ID = i
        tutor_ids = studentdata.get('Tutor_Id')
        for j in tutor_ids:
            tutorname = getname(j)
            tutordata = gettutor(j)
            for k in tutordata['Courses']:
                cid = k['CourseId']
                if(cid == course_ID):
                    listofid.append(course_ID)
                    data = syllabus(course_ID)
                    tutordetails = collectdata(tutorname, j, data, course_ID)
                    print(tutordetails)
    print("total contains data issssssssssssssssssssssssiiiiiiiiiiiii", tutordetails)
    return tutordetails, True

def collectdata(t_name, t_id, t_course, course_id):
    '''pushing student data into one list'''
    courses = {}
    courses["Tutor_Name"] = t_name
    courses["Tutor_Id"] = t_id
    courses["Course"] = t_course
    totaldetails.append({course_id:courses})
    return totaldetails

#@app.route('/syllabus/<ab>',methods=['POST','GET'])
def syllabus(c_id):
    '''getting course syllabus by id '''
#    print("syllabus data" ,ab)
    try:
        table = DBRESOURCE.Table("CourseSyllabus.Dev")
        response = table.get_item(
            Key={
                'Course_Id':str(c_id)
                })
        data = response['Item']
        return data
    except ClientError as e:
        logging.error(e)
        return "invalid"

def gettutor(tid):
    '''
    tutor courses data
    '''
#    print("tutor id is",tid)
    try:
        table = DBRESOURCE.Table("TutorCourses.Dev")
        response = table.get_item(
            Key={
                'Tutor_Id':tid
                })
        tdata = response['Item']
        return tdata
    except ClientError as e:
        logging.error(e)
        return "no tutor"

def getname(tid):
    '''
    for getting tutor name
    '''
#    print("tutor id is",tid)
    table = DBRESOURCE.Table('TutorPersonalInfo.Dev')
    response = table.query(
        KeyConditionExpression=Key('Tutor_Id').eq(tid))
    items = response['Items']
    tname = items[0]['Name']
    return tname

if __name__ == '__main__':
    PORT = 8089
    HTTP_SERVER = WSGIServer(('0.0.0.0', PORT), APP)
    print("Running on port", PORT)
    HTTP_SERVER.serve_forever()
