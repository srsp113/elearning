# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 12:37:51 2020

@author: ssai
"""


import io
import logging
import boto3
import random
from flask import Flask, request,jsonify,session
from gevent.pywsgi import WSGIServer
from flask_cors import CORS
from botocore.exceptions import ClientError

app=Flask(__name__)
CORS(app)
dbdata=boto3.resource('dynamodb')
bucketdata=boto3.client('s3')

@app.route('/addCourse',methods=['POST','GET'])
def addcourse():
    '''getting new course details '''
    if request.method=="POST":
        T_id=request.form.get('tid')
        c_name=request.form.get('cname')
        Experience=request.form.get('exp')
        c_fee=request.form.get('fee')
        randomid=str(random.randrange(1,1000))
        c_id=int(str(T_id)+randomid)
        newcourse=addnew(T_id,c_name,Experience,c_fee,c_id)
        print(newcourse)
    return "succesfully Created"
        
def addnew(T_id,c_name,exp,c_fee,c_id):
    print(T_id,c_name,exp,c_fee,c_id)
    table=dbdata.Table('TutorCourses.Dev')
    response=table.update_item(
            Key={
                    'Tutor_Id':T_id
                    },
            UpdateExpression="set Courses=list_append(Courses,:course)",
            ExpressionAttributeValues={
                    ':course':[{'Coursefee':c_fee,
                       'CourseId':c_id,
                       'language':c_name,
                       'Experience':exp,
                       'Students':[]
                      }
                            ]
                        }
            )
    return "Sucess"
    
if __name__=='__main__':
    port=8089
    HTTP_SERVER=WSGIServer(('0.0.0.0',port),app)
    print("running on port is",port)
    HTTP_SERVER.serve_forever()