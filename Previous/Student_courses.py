# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 09:39:42 2020

@author: ssai
"""

import logging
from flask import Flask, request, jsonify
from gevent.pywsgi import WSGIServer
from flask_cors import CORS
import boto3
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key

app = Flask(__name__)
CORS(app)
DBACCESS = boto3.resource("dynamodb")
listofid = []
totaldetails = []

@app.route('/course', methods=['POST', 'GET'])
def coursedetail():
    '''for student total course details '''
    if request.method == 'POST':
        totaldata = []
        sid = request.form.get('id')
        table = DBACCESS.Table("StudentCourseDetail.Dev")
        try:
            response = table.get_item(
                Key={
                    'Student_Id':sid
                    })
            studentdata = response['Item']
            totaldata.append(studentdata)
            courseids = course(studentdata)
            totaldata.append(courseids)
            if(courseids):
                print(listofid)
        except ClientError as e:
            logging.error(e)
            return "invalid data"
    return jsonify(totaldata)

def course(studentdata):
    '''checking and getting syllabus '''
    course_ids = studentdata.get('Courses')
#    print("Courseidsss are ",course_ids)
    for i in course_ids:
        course_ID = i
        tutor_ids = studentdata.get('Tutor_Id')
#        print("tutor idsssss are ", tutor_ids)
        for j in tutor_ids:
            tutorname = getname(j)
            tutordata = gettutor(j)
#            tutorskill = tutordata['Skill']
            for k in tutordata['Courses']:
                cid = k['CourseId']
#                print("data is aretttttttttt    ",k)
                if(cid == course_ID):
                    listofid.append(course_ID)
                    data = syllabus(course_ID)
                    print("dataaaaa",data)
                    tutordetails = collectdata(tutorname, j, data, course_ID)
                    print(tutordetails)
    print("total contains data issssssssssssssssssssssssiiiiiiiiiiiii", tutordetails)
    return tutordetails, True

def collectdata(t_name, t_id, t_course, course_id):
    '''pushing student data into one list'''
    courses = {}
    courses["Tutor_Name"] = t_name
    courses["Tutor_Id"] = t_id
    courses["Course"] = t_course
    totaldetails.append({course_id:courses})
    return totaldetails

#@app.route('/syllabus/<ab>',methods=['POST','GET'])
def syllabus(c_id):
    '''getting course syllabus by id '''
#    print("syllabus data" ,ab)
    try:
        table = DBACCESS.Table("CourseSyllabus.Dev")
        response = table.get_item(
            Key={
                'Course_Id':str(c_id)
                })
        data = response['Item']
        return data
    except ClientError as e:
        logging.error(e)
        return "invalid"

def gettutor(tid):
    '''
    tutor courses data
    '''
#    print("tutor id is",tid)
    try:
        table = DBACCESS.Table("TutorCourses.Dev")
        response = table.get_item(
            Key={
                'Tutor_Id':tid
                })
        tdata = response['Item']
        return tdata
    except ClientError as e:
        logging.error(e)
        return "no tutor"

def getname(tid):
    '''
    for getting tutor name
    '''
#    print("tutor id is",tid)
    table = DBACCESS.Table('TutorPersonalInfo.Dev')
    response = table.query(
        KeyConditionExpression=Key('Tutor_Id').eq(tid))
    items = response['Items']
    print(items)
    tname = items[0]['Name']
    return tname

if __name__ == '__main__':
    PORT = 8086
    HTTP_SERVER = WSGIServer(('0.0.0.0', PORT), app)
    print("running on port", PORT)
    HTTP_SERVER.serve_forever()
    