# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 09:32:01 2020

@author: ssai
"""

import io
import logging
import boto3
import random
from flask import Flask, request,jsonify,session
from gevent.pywsgi import WSGIServer
from flask_cors import CORS
from botocore.exceptions import ClientError

app=Flask(__name__)
CORS(app)
dbdata=boto3.resource('dynamodb')
bucketdata=boto3.client('s3')

@app.route('/getstudent',methods=['POST','GET'])
def addcourse():
    if request.method=="POST":
        S_id=request.form.get('sid')
        table=dbdata.Table('StudentCourseDetail.Dev')
        response=table.get_item(
                Key={
                        'Student_Id':S_id
                        })
        item =response['Item']
    return item
        
        
#def addnew(T_id,c_name,exp,c_fee,c_id):
#    print(T_id,c_name,exp,c_fee,c_id)
#    table=dbdata.Table('TutorCourses.Dev')
#    response=table.update_item(
#            Key={
#                    'Tutor_Id':T_id
#                    },
#            UpdateExpression="set Courses=list_append(Courses,:course)",
#            ExpressionAttributeValues={
#                    ':course':[{'Coursefee':c_fee,
#                       'CourseId':c_id,
#                       'language':c_name,
#                       'Experience':exp,
#                       'Students':[]
#                      }
#                            ]
#                        }
#            )
#    return "Sucess"
#    

#{'CourseId':c_id,
#                             'CourseFee':c_fee,
#                             'Experience':exp,
#                             'language':c_name}
if __name__=='__main__':
    port=8089
    HTTP_SERVER=WSGIServer(('0.0.0.0',port),app)
    print("running on port is",port)
    HTTP_SERVER.serve_forever()