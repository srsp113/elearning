# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 16:52:37 2020

@author: ssai
"""

import boto3
from flask import Flask, request, jsonify
from gevent.pywsgi import WSGIServer
from flask_cors import CORS
from botocore.exceptions import ClientError

app = Flask(__name__)
CORS(app)
dbdata = boto3.resource('dynamodb')
bucketdata = boto3.client('s3')

@app.route('/addnew', methods=['POST', 'GET'])
def addcourse():
    '''adding new course details '''
    if request.method == "POST":
        S_id = request.form.get("s_id")#get from background process only not from form
        T_id = request.form.get("t_id")
        C_id = request.form.get("c_id")
        newcourse = addnew(S_id, T_id, C_id)
    return newcourse

def addnew(S_id, T_id, C_id):
    '''adding ids only '''
    print(S_id, T_id, C_id)
    table = dbdata.Table('StudentCourseDetail.Dev')
    try:
        response = table.update_item(
            Key={
                'Student_Id':S_id
                },
            UpdateExpression="set Courses=list_append(Courses,:c), Tutor_Id=list_append(Tutor_Id,:t)",
            ExpressionAttributeValues={
                ':c':[int(C_id)],
                ':t':[int(T_id)]
                }
            )
        return "Success"
    except:
        return "Something Went Wrong"
    
if __name__ == '__main__':
    PORT = 8089
    HTTP_SERVER = WSGIServer(('0.0.0.0', PORT), app)
    print("running on port is", PORT)
    HTTP_SERVER.serve_forever()
    