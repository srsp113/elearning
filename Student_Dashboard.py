# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 10:12:53 2020

@author: ssai
"""

import boto3
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key
from flask import Flask, request, jsonify
from gevent.pywsgi import WSGIServer
from flask_cors import CORS

APP = Flask(__name__)
CORS(APP)
DBRESOURCE = boto3.resource("dynamodb")
S3BUCKET = boto3.client("s3")
coursedata = DBRESOURCE.Table("Course_Details")

@APP.route('/login', methods =['POST', 'GET'])
def student_dashboard():
    if request.method == 'POST':
        logincred = request.get_json()
        username = logincred.get('user')
        password = logincred.get('passwd')
        print(username, password)
        creddetails = DBRESOURCE.Table('Credentials')
        resp = creddetails.query(
                KeyConditionExpression = Key('Email_Id').eq(username))
        tabledata = resp['Items']
        print(tabledata)
        user = tabledata[0]['Email_Id']
        passs = tabledata[0]['Password']
        role = tabledata[0]['Role']
        if(username == user and password == passs):
            if(role =='Student'):
                data = dashboard(user)
                coursesearech =searchTutor()
                print(data)
                print(coursesearech)
                return jsonify(studentdata=data,newsearch =coursesearech )
            elif(role == 'Tutor'):
                print("Tutor data ") 
        return "s"

def dashboard(emailid):
    student_detail=DBRESOURCE.Table('Student_Details')
    response = student_detail.query(
            KeyConditionExpression = Key('Student_Email').eq(emailid))
    student_data = response['Items']
    courseids = student_data[0]['CourseIds']
    course_details = coursedetail(courseids)
    print("courseeeeeeeeeeeeeeeeeeee list",course_details)
#    dropdownlist = dropdown(coursids)
    return student_data,course_details

def coursedetail(ids):
    totallist=[]
    listt=[]
    for i in ids:
        response = coursedata.query(
        KeyConditionExpression = Key("Course_Id").eq(i))
        course = response['Items']
        for j in course:
            d={}
            d['CourseId']=i
            d['Coursename']=j['Course_Name']
            d['TutorName'] =j['Tutor_Name']
            d['TutorLevel']=j['Tutor_Level']
            d['duration']=j['Course_Duration']
            totallist.append(d)
            c_list={}
            c_list['CourseId']=i
            c_list['Courses']=j['Course_Name']
            listt.append(c_list)           
    return totallist,listt

def searchTutor():
    coursetutor = DBRESOURCE.Table("Course_Details")
    response = coursetutor.scan()
    data = response['Items']
    courselist=[]
    totaldetails=[]
    for i in data:
        coursename =i['Course_Name']
        courselist.append(coursename.lower())  
    courselist = set(courselist)
    for i in courselist:
        listof=[]
        totaldetails.append({i:listof})
        for j in data:
            jid=j['Course_Name']
            if(i==jid.lower()):
                name=j['Tutor_Name']
                c_id =j['Course_Id']
                listof.append({name:c_id})
    return totaldetails

@APP.route('/viewdetails', methods = ['POST', 'GET'])
def viewcoursedetails():
    if request.method == 'POST':
#        courseidjson = request.get_json()
        courseid = request.form.get("cid")
        resp = coursedata.query(
                KeyConditionExpression = Key("Course_Id").eq(courseid))
        coursedetails = resp['Items']
        viewdetail = coursedetails[0]["Course_Detail"][0]['Content']
        print(viewdetail)
        print(coursedetails)
    return jsonify(showdetails = viewdetail)

@APP.route('/addCourse', methods = ['POST', 'GET'])
def addnewcourse():
    if request.method == 'POST':
#        jsondata = request.get_json()
        studenttable = DBRESOURCE.Table("Student_Details")
        courseid = request.form.get("cid")
        studentid = request.form.get("sid")
        response = studenttable.update_item(
            Key={
                    'Student_Email':studentid
                    },
            UpdateExpression = "set CourseIds = list_append(CourseIds, :c)",
            ExpressionAttributeValues = {
                    ':c':[courseid]
                        }
                    )
        return "Sucess"


#@APP.route('/datacheck', methods =['POST', 'GET'])
#def datacheck():
#    if request.method =="POST":
#        date = request.get_json()
#        searchdate = date.get("date")
        
       
if __name__ == '__main__':
    PORT = 8083
    HTTP_SERVER = WSGIServer(('0.0.0.0', PORT), APP)
    print("Running on Port is :",PORT)
    HTTP_SERVER.serve_forever()